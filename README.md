# Thousand Server Fetcher

Program that asynchronously queries the statuses of 1000 fictitious servers and produces a report of statuses. 

In this toy case, the program sends get requests to a `<provided-endpoing>/<server-name>`. For example,
`http://localhost:8080/server/health/server-0001`. Responses looks like the following.   
```
{"Application":"Cache2","Version":"1.0.1","Uptime":4637719417,"Request_Count":5194800029,"Error_Count":1042813251,"Success_Count":4151986778},
```
If it fails to get a response, it will retry a limited number of time with exponential backoff. If it fails, this 
failure will be logged and the program will continue. It produces a report that aggregates the success rate by application by version.
It prints it out in the console and writes it in csv and parquet format.

## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites
* Java 8

### Quick Start

#### Run with Java only
1. Download the executable, the Java jar at `core/target/scala-2.13/health-reporter-core.jar`
2. Run program by running jar and providing appropriate parameters. See Usage section below for more details and 
   examples.  
   `java -jar core/target/scala-2.13/core-assembly.jar <server-health-api-endpoint> <report-write-directory>`

#### Run with SBT (For Development)
0. Install [Simple Build Tool (SBT)](https://www.scala-lang.org/download/)
1. Download the whole repository for this program.
2. `sbt run <server-health-api-endpoint> <report-write-directory>`


## Demo
`java -jar core/target/scala-2.13/core-assembly.jar <server-health-api-endpoint> <report-write-directory>`

```bash
# stand up development server
java -jar dev/target/scala-2.13/health-reporter-dev-assembly.jar 8099

# IN ANOTHER TERMINAL
java -jar core/target/scala-2.13/health-reporter-core.jar target http://localhost:8099/server/health 
[info] running vivian.serverhealthreporter.Main http://localhost:8089/server/health target
SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
requesting server health from http://localhost:8099/server/health...
requesting server-0995 health from http://localhost:8099/server/health...
requesting server-0981 health from http://localhost:8099/server/health...
< elided>
Cache0
  "0.0.1"         58 %
  "0.0.2"         53 %
< elided> 
Webapp0
  "0.0.1"         50 %

log4j:WARN No appenders could be found for logger (org.apache.htrace.core.Tracer).
log4j:WARN Please initialize the log4j system properly.
log4j:WARN See http://logging.apache.org/log4j/1.2/faq.html#noconfig for more info.
[success] Total time: 3 s, completed Apr 17, 2022 6:12:14 PM

cat target/health-report.csv
Cache0,0.0.1,0.5840623
Cache0,0.0.2,0.532864
Cache1,1.2.0,0.44607425
< elided >

parquet-tools cat target/health-report.pq
applicationName = Cache0
version = 0.0.1
successRate = 0.5840623

applicationName = Cache0
version = 0.0.2
successRate = 0.532864
< elided >
```

## Roadmap
- enable passing in named arguments with [scallop](https://github.com/scallop/scallop)
- better warning, logging of failures to get responses
- better error handling with stronger typing
  - smart constructors
    ```scala
    final case class Application(name: ApplicationName)
    sealed abstract case class ApplicationName private (value: String)
    object ApplicationName {
      def fromString(s: String): Option[ApplicationName] =
        if (s == null || s.isEmpty) None
        else Some(new ApplicationName(s) {})
    }
    // ApplicationName.fromString("App0").map(Application(_))    
    ```
  - stronger typing of particular classes necessitate custom `parquet4s` 
    [codecs](https://mjakubowski84.github.io/parquet4s/docs/records_and_schema/)
- integration tests
- unit tests for every possible invalid response in [ServerHealthResponseTest](src/test/scala/vivian/serverhealthreporter/ServerHealthResponseTest.scala)
    

## Development
While this information will very likely apply on most Linux machines, they have only been used on MacOS and 
adjustments may be necessary.

### Prerequisites
This is an example of how to list things you need to use the software and how to install them.
* Java 8
* [Simple Build Tool (SBT)](https://www.scala-lang.org/download/)
* The whole repository for this program, not just the executable jar mentioned in Installation section

### Example Commands
Command Line
```bash
# use Java 8 if not already # Just one way a system can be configured 
# to use Java 8. Your system may be different
export JAVA_HOME=$(/usr/libexec/java_home -v 1.8)

sbt 'core/test' # run tests

# optional. You can stand up a toy server to ping during development. See details in section below
sbt 'dev/run 8088' # sbt 'dev/run <local-port>'

# IN ANOTHER TERMINAL, you can test  toy server
curl http://localhost:8088/server/health/server-0200
#{"Application":"Cache0","Version":"0.2.1","Uptime":6711085858,"Request_Count":9698317225,"Error_Count":4769325245,"Success_Count":4928991980}%

sbt 'core/run target/ http://localhost:8088/server/health'
```

### Test Server
```bash
sbt 'dev/run' # run test server
```

This is not meant for production. `http4s` does not provide a stub service out of the box. I could develop without a 
test server, but in practice, there has always been a real server that I could ping. It is just easier to stand up a 
small, test server.

## Built With

* [IntelliJ Idea](https://www.jetbrains.com/idea/) - Code Editor
* [SBT](https://www.scala-sbt.org/) - Dependency Management
* [sbt-assembly](https://github.com/sbt/sbt-assembly) - Build uber jar
* [VisualVM](https://visualvm.github.io/index.html) - Load testing
* [parquet-tools](https://pypi.org/project/parquet-tools/) - Command line interface tool to view parquet files
  `pip install parquet-tools # or brew install parquet-tools`
* Apple Darwin 21.4.0 and Mac OS 12.3

<span style="opacity: 0;">
transparent because only useful to author...
- case classes with no methods "skinny case classes" ie type classes are more functional, but would needlessly 
  over-complicate project
## Resources from Development
https://stackoverflow.com/a/10835527/9500134
https://twitter.github.io/effectivescala/#Functional%20programming
https://stackoverflow.com/questions/36044489/scala-typeclass-derivation-for-collections
https://stackoverflow.com/questions/5408861/what-are-type-classes-in-scala-useful-for
https://basicobject.medium.com/selectively-excluding-dependency-in-sbt-61b91070843
https://www.scalatest.org/user_guide/using_scalatest_with_sbt
https://scalameta.org/scalafmt/docs/configuration.html
https://mjakubowski84.github.io/parquet4s/docs/records_and_schema/
https://github.com/twitter/finatra/blob/develop/inject/inject-logback/src/test/resources/logback-test.xml
https://softwaremill.com/practical-guide-to-error-handling-in-scala-cats-and-cats-effect/#ensure
https://typelevel.org/blog/2020/10/30/concurrency-in-ce3.html#:~:text=The%20most%20basic%20action%20of,interleaved%20in%20a%20nondeterministic%20fashion.
https://http4s.org/v0.23/docs/testing.html
https://http4s.org/v0.23/docs/service.html
https://http4s.org/v0.23/docs/client.html
https://typelevel.org/cats-effect/docs/tutorial
</span>
