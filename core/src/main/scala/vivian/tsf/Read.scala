package vivian.tsf

import cats.effect.IO
import cats.implicits.catsSyntaxParallelTraverse1
import io.circe.generic.auto.exportDecoder
import org.http4s.circe.jsonOf
import org.http4s.client.dsl.io.http4sClientSyntaxMethod
import org.http4s.{ember, EntityDecoder, Method, Uri}
import org.typelevel.log4cats.SelfAwareStructuredLogger
import org.typelevel.log4cats.slf4j.Slf4jLogger
import retry.RetryPolicies.{exponentialBackoff, limitRetries}
import retry.retryingOnAllErrors

import scala.concurrent.duration.{DurationInt, FiniteDuration}
import scala.io.Source.fromResource
import scala.util.{Failure, Try}

object Read {

  private def client =
    ember.client.EmberClientBuilder
      .default[IO]
      .build

  implicit val responseDecoder: EntityDecoder[IO, ServerHealthResponse] =
    jsonOf[IO, ServerHealthResponse]

  def requestHealth(
      endpoint: String,
      server: String
  ): IO[ServerHealthResponse] = for {
    _ <- IO.println(s"requesting $server health from $endpoint...")
    resp <- client.use {
      _.expect[ServerHealthResponse](
        Method.GET(
          Uri
            .fromString(
              s"${Utils.dropTrailing(separator = "/")(endpoint)}/$server"
            )
            .getOrElse(Uri.uri(""))
        )
      )(responseDecoder)
    }
  } yield resp

  def getAllServersHealth(
      endpoint: String,
      nRetries: Int = 4,
      backoffTime: FiniteDuration = 15.milliseconds
  ): IO[Seq[(String, Try[ServerHealthResponse])]] = for {
    servers <- IO.blocking(fromResource("servers.txt").getLines)
    logger  <- Slf4jLogger.create[IO]
    responses <- servers.toSeq
      .parTraverse { s =>
        retryingOnAllErrors(
          limitRetries[IO](nRetries).join(
            exponentialBackoff[IO](backoffTime)
          ),
          onError = retry.noop[IO, Throwable]
        )(requestHealth(endpoint, s).map(r => (s, Try(r)))).handleError { e =>
          val msg =
            s"failed to get health status for server $s after $nRetries tries; $e"
          logger.warn(msg)
          println(msg)
          (s, Failure(e))
        }
      }
  } yield responses

}
