package vivian.tsf

import cats.effect.unsafe.implicits.global
import cats.effect.{ExitCode, IO, IOApp}
import org.typelevel.log4cats.slf4j.Slf4jLogger
import vivian.tsf.ServerHealthResponse.{clean, reportRows}

import scala.concurrent.duration.DurationInt

object Main extends IOApp {

  def run(args: List[String]): IO[ExitCode] = {
    val defaultEndpoint = "http://localhost:8080/server/health"
    val writeDirectory = IO(
      args.headOption
        .getOrElse(
          throw new IllegalArgumentException(
            "Provide second parameter, the directory to write the health report files to."
          )
        )
    )
    val etl = for {
      endpoint       <- IO(args.lift(1).getOrElse(defaultEndpoint))
      logger         <- Slf4jLogger.create[IO]
      _              <- IO.println(s"""requesting server health from $endpoint...""")
      responses      <- Read.getAllServersHealth(endpoint, nRetries = 5, backoffTime = 1.seconds)
      cleaned        <- IO(clean(responses, logger))
      aggregated     <- IO(reportRows(cleaned))
      report         <- IO(Report(aggregated.toSeq))
      writeDirectory <- writeDirectory
      _              <- IO.println(s"writing report in multiple formats to $writeDirectory...")
      _              <- report.writeAsync(writeDirectory)
    } yield ()
    IO(etl.unsafeRunSync()(runtime = global)).as(ExitCode.Success)
  }
}
