package vivian.tsf

import cats.effect.IO
import org.typelevel.log4cats.Logger

import scala.collection.immutable
import scala.util.{Failure, Success, Try}

final case class ServerHealthResponse(
    Application: String,
    Version: String,
    Uptime: BigInt,
    Request_Count: BigInt,
    Error_Count: BigInt,
    Success_Count: BigInt
) { self =>
  def successRate(): BigDecimal =
    BigDecimal(Success_Count) / BigDecimal(Request_Count)
  def validate(): Either[String, ServerHealthResponse] = {
    val errors =
      self.productElementNames.zip(self.productIterator).foldLeft("") {
        case (acc, (field, value)) =>
          val err = value match {
            case s: String if s.isEmpty =>
              s"field $field value is an empty string"
            case _: String | _: BigInt => ""
            case null                  => s"field $field value is null"
            case v =>
              throw new Exception(
                s"Developer did not expect field value $v. Program error."
              )
          }
          acc + err
      } + {
        if (Error_Count + Success_Count != Request_Count)
          "request count != error + success count"
        else
          ""
      }
    if (errors.isEmpty) Right(self) else Left(errors)
  }
}

object ServerHealthResponse {
  def aggSuccessRate(segment: Seq[ServerHealthResponse]): Float = {
    val sum = segment.foldLeft(BigDecimal(0)) { case (sum, stats) =>
      sum + stats.successRate
    }
    val mean = sum / BigDecimal(segment.length)
    mean.toFloat
  }

  def reportRows(
      validResponses: Iterable[ServerHealthResponse]
  ): immutable.Iterable[ReportRow] =
    validResponses
      .groupBy(stats => (stats.Application, stats.Version))
      .map { case ((app, v), stats) =>
        ReportRow(app, v, aggSuccessRate(stats.toSeq))
      }

  def clean(
      responses: Iterable[(String, Try[ServerHealthResponse])],
      logger: Logger[IO]
  ): Iterable[ServerHealthResponse] =
    responses.flatMap {
      case (_, Failure(_)) => None
      case (server, Success(r)) =>
        r.validate match {
          case Right(r) => Some(r)
          case Left(err) =>
            val msg =
              s"Filtered out invalid response from $server: $r. Invalid " +
                s"because $err"
            logger.warn(msg)
            println(msg)
            None
        }
    }
}
