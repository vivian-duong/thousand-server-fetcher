package vivian.tsf

import cats.effect.{IO, Resource}

import java.io.{BufferedWriter, File, FileWriter}

final case class ReportRow(
    applicationName: String,
    version: String,
    successRate: Float
)

final case class Report(rows: Seq[ReportRow]) { self =>
  def print(
      nDecimals: Int = 0,
      roundingMode: BigDecimal.RoundingMode.Value = BigDecimal.RoundingMode.HALF_DOWN
  ): Unit = {
    def prettyAppString(
        applicationName: String,
        healthRateRows: Seq[ReportRow]
    ): String =
      applicationName + "\n" +
        healthRateRows.sortBy(_.version).foldLeft("") { case (s, ReportRow(_, version, rate)) =>
          s + s"""\t"$version"\t\t${BigDecimal(rate * 100)
            .setScale(nDecimals, roundingMode)} %\n"""
        }
    val prettyString = rows
      .groupBy(_.applicationName)
      .toSeq
      .sortBy { case (app, _) => app }
      .foldLeft("") { case (acc, (app, versionStats)) =>
        acc + prettyAppString(app, versionStats)
      }
    println(prettyString)
  }

  def csv(): String =
    rows.foldLeft("") { case (acc, r) =>
      acc + Utils.toCSV(r) + "\n"
    }

  /**
   * writeCSV writes report as a CSV file
   *
   * @param writeDirectory
   * @param fileName
   * @return
   *   `IO[Unit]` I normally prefer to write Cats library agnostic functions that I can wrap in a
   *   `Cats` `IO`. But in this case, I need a guarantee that `BufferedWriter` closes and because I
   *   will use `Cats`` anyway, having `Cats`` handle this will better integrate with its running of
   *   my program. `IO.blocking` signals that the wrapped code block will block a thread, so it is
   *   good practice for input/output actions. This information helps `Cats-effect` optimize `IO`
   *   scheduling.
   */
  def writeCSV(
      writeDirectory: String,
      fileName: String = "health-report.csv"
  ): IO[Unit] = {
    val writePath = writeDirectory + "/" + fileName
    val writer = Resource.make {
      IO.blocking(
        new BufferedWriter(
          new FileWriter(new File(writePath))
        )
      )
    } { fileWriter =>
      IO.blocking(fileWriter.close()).handleErrorWith(_ => IO.unit) // release
    }
    IO.println(s"writing csv to $writePath") &> writer.use(bw => IO.blocking(bw.write(self.csv)))
  }

  def writeParquet(
      writeDirectory: String,
      fileName: String = "health-report.pq"
  ): Unit =
    Utils.writeParquet[ReportRow](
      rows = self.rows,
      writePath = s"$writeDirectory/$fileName"
    )

  def writeAsync(
      writeDirectory: String,
      fileNamePq: String = "health-report.pq",
      fileNameCSV: String = "health-report.csv"
  ): IO[Unit] =
    IO(print()) &>
      writeCSV(writeDirectory, fileNameCSV) &>
      IO(writeParquet(writeDirectory, fileNamePq))
}
