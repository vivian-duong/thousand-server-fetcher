package vivian.tsf

import com.github.mjakubowski84.parquet4s.ParquetWriter.ParquetWriterFactory
import com.github.mjakubowski84.parquet4s.{ParquetWriter, Path}
import org.apache.parquet.hadoop.ParquetFileWriter
import org.apache.parquet.hadoop.metadata.CompressionCodecName

object Utils {

  /**
   * toCSV returns comma separated `Product` values string. Note that this does not account for edge
   * cases. CSV is ad-hoc and unreliable. If there are a lot of edge cases, consider a different
   * data format such as Avro.
   *
   * @param prod
   * @return
   *   csv string where values are `prod`'s values
   */
  def toCSV(prod: Product): String = prod.productIterator
    .map {
      case Some(value) => value
      case None        => ""
      case rest        => rest
    }
    .mkString(",")

  def writeParquet[A](rows: Iterable[A], writePath: String)(implicit
      pwf: ParquetWriterFactory[A]
  ): Unit =
    ParquetWriter.writeAndClose(
      Path(writePath),
      rows,
      ParquetWriter.Options(
        writeMode = ParquetFileWriter.Mode.OVERWRITE,
        compressionCodecName = CompressionCodecName.SNAPPY
      )
    )

  @annotation.tailrec
  def dropTrailing(separator: String = "/")(path: String): String =
    path.takeRight(separator.length) match {
      case sep if sep == separator =>
        dropTrailing(separator)(
          path.dropRight(separator.length)
        )
      case _ => path
    }
}
