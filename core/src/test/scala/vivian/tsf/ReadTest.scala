package vivian.tsf

import cats.effect.unsafe.implicits.global
import cats.implicits._
import io.circe.generic.auto._
import org.http4s.circe.CirceEntityCodec.circeEntityDecoder
import org.http4s.dsl.io.{http4sOkSyntax, Ok}
import org.scalatest.funsuite.AnyFunSuite
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

import scala.util.Try

class ReadTest extends AnyFunSuite with ScalaCheckPropertyChecks {
  def time[R](block: => R): R = {
    val t0     = System.nanoTime()
    val result = block // call-by-name
    val t1     = System.nanoTime()
    println("Elapsed time: " + (t1 - t0) + "ns")
    result
  }

  val stringResponses =
    scala.io.Source.fromResource("testcases/responses-edited.txt").getLines

  val encodedResponses: Try[Seq[ServerHealthResponse]] =
    time {
      Try(
        stringResponses.toSeq.parTraverse(Ok(_).flatMap(_.as[ServerHealthResponse])).unsafeRunSync
      )
    }

  test("Circe encodes valid json responses") {
    assert {
      encodedResponses.isSuccess
    }
  }

  test("Circe does *not* be able to encode unexpected responses as case classes") {
    Ok("""{"Application":999,"Version":"0.2.2","Uptime":0,"Request_Count":,
        |"Error_Count":0,"Success_Count":0}""".stripMargin)
      .flatMap(_.as[ServerHealthResponse])
      .map {
        case _: ServerHealthResponse => false
        case _                       => true
      }
  }

}
