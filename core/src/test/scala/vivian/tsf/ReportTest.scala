package vivian.tsf

import org.scalacheck.Gen
import org.scalatest.funsuite.AnyFunSuite
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class ReportTest extends AnyFunSuite with ScalaCheckPropertyChecks {
  val genReportRows: Gen[ReportRow] = for {
    letters <- Gen.alphaStr
    digit   <- Gen.numChar
    nums    <- Gen.listOf(Gen.numChar)
    app     = letters + digit
    version = (digit :: '.' :: nums).mkString
    rate <- Gen.chooseNum(0, 1)
  } yield ReportRow(app, version, rate)

  val genReport: Gen[Report] = for {
    rows <- Gen.containerOf[Seq, ReportRow](genReportRows)
  } yield Report(rows)

  val mockReport: Report = Report(Seq(ReportRow("App1", "1.2.1", 0.88888888888f)))

  test("Report.csv returns the correct number of rows") {
    forAll(genReport) { r =>
      assert(r.csv.count(_ == '\n') == r.rows.length)
    }
  }

  test("Report.csv returns a csv string") {

    assert(mockReport.csv == "App1,1.2.1,0.8888889\n")
  }

}
