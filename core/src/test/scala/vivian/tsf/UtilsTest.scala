package vivian.tsf

import org.scalacheck.Gen
import org.scalatest.funsuite.AnyFunSuite
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class UtilsTest extends AnyFunSuite with ScalaCheckPropertyChecks {
  val shortStrings: Gen[String] = Gen.stringOfN(10, Gen.alphaNumChar)
  val myGen: Gen[String] = for {
    shortString <- Gen.stringOfN(10, Gen.alphaNumChar)
    sep         <- Gen.frequency((7, "/"), (3, ""))
  } yield shortString + sep

  test("dropTrailing returns no trailing separator") {
    forAll(shortStrings) { p: String =>
      assert(!Utils.dropTrailing(separator = "/")(path = p).endsWith("/"))
    }
  }

  test("dropTrailing only drops trailing separators ") {
    forAll(myGen) { p: String =>
      val result = Utils.dropTrailing("/")(path = p)
      assert(!p.contains('/') || (p.contains('/') && !result.endsWith("/")))
    }
  }
}
