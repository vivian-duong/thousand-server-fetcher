package vivian.tsf

import org.scalacheck.{Arbitrary, Gen}
import org.scalatest.funsuite.AnyFunSuite
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

import scala.math.BigInt

class ServerHealthResponseTest extends AnyFunSuite with ScalaCheckPropertyChecks {
  def genBigInt: Gen[BigInt] = {
    val offset = -BigInt(Long.MinValue)
    Arbitrary.arbitrary[Long].map(BigInt(_) + offset)
  }

  val genValidResponses: Gen[ServerHealthResponse] = for {
    letters <- Gen.alphaStr
    digit   <- Gen.numChar
    nums    <- Gen.listOf(Gen.numChar)
    app = letters + digit
    v   = (digit :: '.' :: nums).mkString
    up      <- genBigInt
    success <- genBigInt
    error   <- genBigInt
  } yield ServerHealthResponse(
    app,
    v,
    up,
    Request_Count = success + error,
    Error_Count = error,
    Success_Count = success
  )

  test(
    "ServerHealthResponse.validate() correctly identifies provided valid responses"
  ) {
    assert(
      new ReadTest().encodedResponses
        .map(_.forall(_.validate.isRight))
        .getOrElse(true)
    )
  }

  test(
    "ServerHealthResponse.validate() correctly identifies generated valid responses"
  ) {
    forAll(genValidResponses) { r: ServerHealthResponse =>
      assert(r.validate.isRight, s"failed at $r")
    }
  }

  test(
    "ServerHealthResponse.validate() correctly identifies *in*valid responses where request count != success + error " +
      "count"
  ) {
    val gen = for {
      nums    <- Gen.listOf(Gen.numChar)
      letters <- Gen.alphaStr
      digit   <- Gen.numChar
      app = letters + digit
      v   = (digit :: nums).mkString
      up      <- genBigInt
      success <- genBigInt
      error   <- genBigInt
      count   <- genBigInt
    } yield ServerHealthResponse(
      app,
      v,
      up,
      Request_Count = count,
      Error_Count = error,
      Success_Count = success
    )
    forAll(gen) { r: ServerHealthResponse =>
      assert(r == null || r.validate.isLeft, s"failed at $r")
    }
  }

}
