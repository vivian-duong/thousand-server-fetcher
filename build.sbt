ThisBuild / version      := "0.1"
ThisBuild / scalaVersion := "2.13.8"

// http4s depends on Java 8
ThisBuild / javacOptions ++= Seq("-target", "1.8", "-source", "1.8")
ThisBuild / scalacOptions ++=
  Seq(
    "-target:jvm-1.8"
  )

val http4sVersion = "0.23.11"
val circeVersion  = "0.14.1"

lazy val dev = (project in file("dev")).settings(
  name := "thousand-server-health-fetcher-dev",
  libraryDependencies ++= Seq(
    "org.http4s" %% "http4s-dsl"          % http4sVersion,
    "org.http4s" %% "http4s-ember-server" % http4sVersion
  ),
  assembly / assemblyJarName := s"${name.value}.jar"
)

lazy val core = (project in file("core")).settings(
  name := "thousand-server-health-fetcher-core",
  libraryDependencies ++= Seq(
    "org.http4s" %% "http4s-dsl"          % http4sVersion,
    "org.http4s" %% "http4s-ember-client" % http4sVersion,
    "org.http4s" %% "http4s-circe"        % http4sVersion,
    "io.circe"   %% "circe-generic"       % circeVersion, // auto-derivation of JSON codec
    "io.circe"   %% "circe-literal"       % circeVersion, // string interpolation to JSON model
    "com.github.cb372"         %% "cats-retry"         % "3.0.0",
    "org.typelevel"            %% "log4cats-slf4j"     % "2.2.0",
    "org.scalacheck"           %% "scalacheck"         % "1.15.4"   % "test",
    "org.scalatestplus"        %% "scalacheck-1-15"    % "3.2.11.0" % "test",
    "org.scalatest"            %% "scalatest-funsuite" % "3.2.11"   % "test",
    "com.github.mjakubowski84" %% "parquet4s-core"     % "2.4.1",

    // resolve dependency conflict; exclude / evict javax.xml.bind:jaxb-api:2.2.2 for version 2.2.11
    ("org.apache.hadoop" % "hadoop-client" % "3.3.1")
      .exclude(org = "javax.xml.bind", name = "jaxb-api")
      .exclude("com.fasterxml.jackson.core", "jackson-databind") // exclude v 2.10.5 for 2.10.5.1
      .exclude(
        "org.codehaus.jackson",
        "jackson-core-asl"
      ) // org.codehaus.jackson:jackson-core-asl:1.9.2 (evicted by
    // 1.9.13)
  ),
  assembly / assemblyJarName := s"${name.value}.jar",
  assembly / mainClass       := Some("vivian.serverhealthreporter.Main"),
  assemblyMergeStrategy in assembly := {
    // `module-info.class` file has moved into many libraries
    //  only Java 9+ uses `module-info.class` and this proj requires Java 8 so it is safe to discard
    case x if x.endsWith("module-info.class") => MergeStrategy.discard
    case x =>
      val oldStrategy = (assemblyMergeStrategy in assembly).value
      oldStrategy(x)
  }
)
